
import java.io.*;
import java.util.*;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//import urna.conexoes.TrataVoto;

public class Servidor {

    public static void main(String[] args) throws IOException {
        // inicia o servidor
        new Servidor(8745).executa();
    }

    private int porta, qntBolsonaro, qntMarina, qntCiro, qntAlckmin, qntHaddad, qntBranco, qntNulo;
    private List<PrintStream> clientes;
    private final int Bolsonaro = 17;
    private final int Marina = 18;
    private final int Ciro = 12;
    private final int Alckmin = 45;
    private final int Haddad = 13;
    private final int Branco = -1;
    private final int Nulo = 2;
    Path file;
    String scliente;
    FileWriter fw = null;

    public Servidor (int porta) throws FileNotFoundException, UnsupportedEncodingException {
        this.porta = porta;
        this.clientes = new ArrayList<PrintStream>();
        this.qntBolsonaro =  this.qntMarina = this.qntCiro = this.qntAlckmin = this.qntHaddad = this.qntBranco = this.qntNulo = 0;
        
        //arquivo texto de dado criado
        PrintWriter writer = new PrintWriter("dados.txt", "UTF-8");
        writer.close();
        
        PrintWriter writer2 = new PrintWriter("logServ.txt", "UTF-8");
        writer.close();
        
    }

    public void executa () throws IOException {
        
        //socket do servidor
        ServerSocket servidor = new ServerSocket(this.porta);
        System.out.println("Porta 8745 aberta!");
        
        String sporta = "Porta 8745 aberta!";
        List<String> lines = Arrays.asList(sporta);
        file = Paths.get("logServ.txt");
        Files.write(file, lines, Charset.forName("UTF-8"));

        
        while (true) {
            // aceita um cliente
            //escreveLog("\nCriando Socket");
            Socket cliente = servidor.accept();
            System.out.println("\n\nNova conexao com o cliente " +     
                cliente.getInetAddress().getHostAddress()
            );
            escreveLog("\n\nNova conexao com o cliente " + cliente.getInetAddress().getHostAddress()); 
            
            // cria tratador de cliente/voto numa nova thread
            escreveLog("\nCriando Thread");
            TrataVoto tv =
              new TrataVoto(cliente.getInputStream(), this);
            new Thread(tv).start();
        }

    }

    //metodo para processar de quem eh o voto recebido
    void contabilizaVoto(String voto) throws IOException{

        int aux;
        if(voto.equals("BRANCO")){
            aux = -1;
            
        }else {
            aux = Integer.parseInt(voto);
        }

        switch (aux) {

            case Bolsonaro:

                qntBolsonaro++;
                escreveVoto();
                System.out.println("Voto em Jair Bolsonaro contabilizado");
                
                escreveLog("\nVoto em Jair Bolsonaro contabilizado");

            break;

            case Marina:

                qntMarina++;
                escreveVoto();
                System.out.println("Voto em Marina Silva contabilizado");
                
                escreveLog("\nVoto em Marina Silva contabilizado");
                
            break;

            case Haddad:

                qntHaddad++;
                escreveVoto();
                System.out.println("Voto em Fernando Haddad contabilizado");
                
                escreveLog("\nVoto em Fernando Haddad contabilizado");
                
            break;

            case Ciro:

                qntCiro++;
                escreveVoto();
                System.out.println("Voto em Ciro Gomes contabilizado");
                
                escreveLog("\nVoto em Ciro Gomes contabilizado");
                
            break;

            case Alckmin:

                qntAlckmin++;
                escreveVoto();
                System.out.println("Voto em Geraldo Alckmin contabilizado");
                
                escreveLog("\nVoto em Geraldo Alckmin contabilizado");
                
            break;

            case Branco:

                qntBranco++;
                escreveVoto();
                System.out.println("Voto em branco contabilizado");
                        
                escreveLog("\nVoto em branco contabilizado");
                
            break;

            case Nulo:

                qntNulo++;
                escreveVoto();
                System.out.println("Voto nulo contabilizado");
                
                escreveLog("\nVoto nulo contabilizado");
                
            break;

            default:

                qntNulo++;
                escreveVoto();
                System.out.println("Voto nulo contabilizado");
                
                escreveLog("\nVoto nulo contabilizado");
        }
    }

    //metodo para escrever a quantidade de votos no arquivo texto de dado
    private void escreveVoto() throws IOException {
        String dado1 = qntBolsonaro + " em Jair Bolsonaro\n";
        String dado2 = qntAlckmin + " em Geraldo Alckmin\n";
        String dado3 = qntMarina + " em Marina Silva\n";
        String dado4 = qntCiro + " em Ciro Gomes\n";
        String dado5 = qntHaddad + " em Fernando Haddad\n";
        String dado6 = qntBranco + " em Brancos\n";
        String dado7 = qntNulo + " Nulos\n";
        
        List<String> lines = Arrays.asList(dado1, dado2, dado3, dado4, dado5, dado6, dado7);
        
        file = Paths.get("dados.txt");
        Files.write(file, lines, Charset.forName("UTF-8"));    
    }

    void escreveLog(String string) {
        try{
                fw = new FileWriter("logServ.txt",true);
                fw.write(string);
                fw.close();
            }
            catch(IOException ioe){
                System.err.println("IOException: " + ioe.getMessage());
            }
    }
}