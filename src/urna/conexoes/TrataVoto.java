
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Federico
 */
public class TrataVoto implements Runnable {

   private InputStream cliente;
   private Servidor servidor;

   public TrataVoto(InputStream cliente, Servidor servidor) {
      this.cliente = cliente;
      this.servidor = servidor;
   }

   public void run() {
      
      //Reading the message from the client
      InputStream is = cliente;
      InputStreamReader isr = new InputStreamReader(is);
      BufferedReader br = new BufferedReader(isr);
      String voto = "";
      try {
         voto = br.readLine();
      } catch (IOException ex) {
         Logger.getLogger(TrataVoto.class.getName()).log(Level.SEVERE, null, ex);
      }
      System.out.println("Message received from client is "+voto);
      servidor.escreveLog("\nMessage received from client is "+voto);
      
      //processando voto recebido
       try {
           servidor.contabilizaVoto(voto);
       } catch (IOException ex) {
           Logger.getLogger(TrataVoto.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
    
}
