package urna.classes;

public class Candidato {

    private String candidato;
    private String cargo;
    private String partido;
    private String numCandidato;
    private String estado;
    
    
    public Candidato(){ }
    
    public Candidato(String candidato, String cargo, String partido, String numCandidato, String estado){
        this.candidato = candidato;
        this.cargo = cargo;
        this.partido = partido;
        this.numCandidato = numCandidato;
        this.estado = estado;
    }
    
    
    public String getCandidato(){
        return candidato;
    }
    
    public String getCargo(){
        return cargo;
    }
    
    public String getPartido(){
        return partido;
    }
    
    public String getNumCandidato(){
        return numCandidato;
    }
    
    public String getEstado(){
        return estado;
    }    
     
    public void setCandidato (String candidato){
        this.candidato = candidato;
    }
    
    public void setCargo (String cargo){
        this.cargo = cargo;
    }    
    
    public void setPartido(String partido){
        this.partido = partido;
    }
    
    public void setNumCandidato(String numCandidato){
        this.numCandidato = numCandidato;
    }
    
    public void setEstado(String estado){
        this.estado = estado;
    }    
    
}
