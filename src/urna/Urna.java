package urna;

import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import urna.conexoes.Cliente;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import urna.classes.Funcoes;

/**
 * @author Federico
 * @author Kym
 */
public class Urna extends Application {
    
    int i=0;
    int confirmaControl = 0;
    int num;
    String textFieldText = "";  
    
    @Override
    public void start(Stage primaryStage) throws UnknownHostException, IOException{
        
        
        ////CONSTRUCAO DA INTERFACE
        TextField textField = new TextField();

        textField.setDisable(true);
        textField.setFont(Font.font(25));
        textField.setMaxWidth(140);
        textField.setTranslateX(20);
        textField.setTranslateY(-220);

        
        
        Button um = new Button();
        um.setText("1");
        um.setPrefSize(40, 40);
        um.setTranslateX(-30);
        um.setTranslateY(-160);
        
        um.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "1";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("1", i, textFieldText);
                i++;
            }
        });
        
        Button dois = new Button();
        dois.setText("2");
        dois.setPrefSize(40, 40);
        dois.setTranslateX(20);
        dois.setTranslateY(-160);
        
         dois.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "2";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("2", i, textFieldText);
                i++;
            }
        });
        
        Button tres = new Button();
        tres.setText("3");
        tres.setPrefSize(40, 40);
        tres.setTranslateX(70);
        tres.setTranslateY(-160);
        
         tres.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "3";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("3", i, textFieldText);
                i++;
            }
        });
                
        Button quatro = new Button();
        quatro.setText("4");
        quatro.setPrefSize(40, 40);
        quatro.setTranslateX(-30);
        quatro.setTranslateY(55);
        quatro.setTranslateY(-110);
        
         quatro.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "4";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("4", i, textFieldText);
                i++;
            }
        });
        
        Button cinco = new Button();
        cinco.setText("5");
        cinco.setPrefSize(40, 40);
        cinco.setTranslateX(20);
        cinco.setTranslateY(55);
        cinco.setTranslateY(-110);
        
         cinco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "5";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("5", i, textFieldText);
                i++;
            }
        });
        
        Button seis = new Button();
        seis.setText("6");
        seis.setPrefSize(40, 40);
        seis.setTranslateX(70);
        seis.setTranslateY(55);
        seis.setTranslateY(-110);
        
         seis.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "6";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("6", i, textFieldText);
                i++;
            }
        });
                
        Button sete = new Button();
        sete.setText("7");
        sete.setPrefSize(40, 40);
        sete.setTranslateX(-30);
        sete.setTranslateY(-60);
        
         sete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "7";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("7", i, textFieldText);
                i++;
            }
        });
        
        Button oito = new Button();
        oito.setText("8");
        oito.setPrefSize(40, 40);
        oito.setTranslateX(20);
        oito.setTranslateY(-60);
        
         oito.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "8";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("8", i, textFieldText);
                i++;
            }
        });
        
        Button nove = new Button();
        nove.setText("9");
        nove.setPrefSize(40, 40);
        nove.setTranslateX(70);
        nove.setTranslateY(-60);
        
         nove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "9";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("9", i, textFieldText);
                i++;
            }
        });
                
        Button zero = new Button();
        zero.setText("0");
        zero.setPrefSize(40, 40);
        zero.setTranslateX(21);
        zero.setTranslateY(-10);
        
         zero.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = textFieldText + "0";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("0", i, textFieldText);
                i++;
            }
        });
                
        Button branco = new Button();
        branco.setText("Branco");
        branco.setPrefSize(65, 30);
        branco.setTranslateX(-15);
        branco.setTranslateY(35);
        
         branco.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldText.equals("BRANCO")){       
                    textFieldText = "";
                }
                textFieldText = "BRANCO";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("branco", i, textFieldText);
                i++;
            }
        });
        
        Button corrige = new Button();
        corrige.setText("Corrige");
        corrige.setPrefSize(65, 30);
        corrige.setTranslateX(60);
        corrige.setTranslateY(35);     
        
         corrige.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textFieldText = "";
                textField.setText(textFieldText);
                Funcoes f = new Funcoes();
                f.botoes("corrige", i, textFieldText);
                i++;
            }
        });
                
        Button confirma = new Button();
        confirma.setText("CONFIRMA");
        confirma.setPrefSize(143, 40);
        confirma.setTranslateX(22);
        confirma.setTranslateY(80);
        
        confirma.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                if(confirmaControl == 0){
                    
                    confirmaControl = 1;
                    
                    Button sim = new Button();
                    sim.setText("CONFIRMAR");
                    
                    Button nao = new Button();
                    nao.setText("CANCELAR");
                    
                    Text text = new Text("              CONFIRMAR VOTO EM " + textField.getText() + "?");              
                    
                    final Stage dialog = new Stage();
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    dialog.initOwner(primaryStage);
                    VBox dialogVbox = new VBox(20);
                    dialogVbox.getChildren().add(text);
                    
                    
                    Scene dialogScene = new Scene(dialogVbox, 300, 100);
                    
                    nao.setPrefSize(143, 40);
                    nao.setLayoutX(4);
                    nao.setLayoutY(0);
                    
                    
                    sim.setPrefSize(143, 40);
                    sim.setLayoutX(151);
                    sim.setLayoutY(0);
                    
                    Pane root = new Pane();
                    root.getChildren().add(sim);
                    root.getChildren().add(nao);
                    
                    dialogVbox.getChildren().add(root);
                    
                    dialog.setScene(dialogScene);
                    dialog.show();
                    
                    //SE BOTAO SIM APERTADO
                    sim.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(textFieldText.equals("")){
                            
                            Text text2 = new Text("                        VOTO VAZIO");
                            
                            final Stage dialog2 = new Stage();
                            dialog2.initModality(Modality.APPLICATION_MODAL);
                            dialog2.initOwner(primaryStage);
                            VBox dialogVbox2 = new VBox(20);
                            dialogVbox2.getChildren().add(text2);


                            Scene dialogScene2 = new Scene(dialogVbox2, 300, 100);
                            dialog2.setScene(dialogScene2);
                            dialog2.show();
                            
                        }else{
                            confirmaControl = 0;
                            Funcoes f = new Funcoes();
                            f.botoes("confirma", i, textFieldText);
                            i++;
                            dialog.close(); 
                            
                            //DECLARACAO DE UM NOVO OBJETO DO TIPO CLIENTE
                            Cliente c = null;
                            try {
                                //INSTANCIALIZACAO DO OBJETO CLIENTE CRIADO
                                //c = new Cliente(InetAddress.getByName("127.0.0.1").getHostAddress(), 8745, textFieldText);
                                c = new Cliente(InetAddress.getByName("kymhouse.ddns.net").getHostAddress(), 8745, textFieldText);
                            } catch (UnknownHostException ex) {
                                Logger.getLogger(Urna.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            try {
                                //EXECUCAO DO METODO ESCUTA, CLASSE CLIENTE
                                c.executa();
                            } catch (IOException ex) {
                                Logger.getLogger(Urna.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            textFieldText = "";
                            textField.setText(textFieldText);
                        }
                    }
                    });
                    
                    nao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        
                        confirmaControl = 0;
                        dialog.close();
                    }
                    });                
                }
            }
        });

        Label pres0 = new Label("Lista de Presidentes:");
        Label pres1 = new Label("Presidente Ciro Gomes | Numero: 12");
        Label pres2 = new Label("Presidente Fernando Haddad  | Numero: 13");
        Label pres3 = new Label("Presidente Jair Bolsonaro | Numero: 17");
        Label pres4 = new Label("Presidente Marina Silva | Numero: 18");
        Label pres5 = new Label("Presidente Geraldo Alckmin | Numero: 45");
        
        
        pres0.setTranslateX(-205);
        pres0.setTranslateY(-245);
        pres0.setFont(Font.font(20));
        pres1.setTranslateX(-199);
        pres1.setTranslateY(-200);
        pres2.setTranslateX(-180);
        pres2.setTranslateY(-170);
        pres3.setTranslateX(-194);
        pres3.setTranslateY(-140);
        pres4.setTranslateX(-198);
        pres4.setTranslateY(-110);
        pres5.setTranslateX(-186);
        pres5.setTranslateY(-80);
   
        StackPane root = new StackPane();
        root.getChildren().add(um);
        root.getChildren().add(dois);
        root.getChildren().add(tres);
        root.getChildren().add(quatro);
        root.getChildren().add(cinco);
        root.getChildren().add(seis);
        root.getChildren().add(sete);
        root.getChildren().add(oito);
        root.getChildren().add(nove);
        root.getChildren().add(zero);
        root.getChildren().add(branco);
        root.getChildren().add(corrige);
        root.getChildren().add(confirma);
        root.getChildren().add(textField);
        root.getChildren().add(pres1);
        root.getChildren().add(pres2);
        root.getChildren().add(pres3);
        root.getChildren().add(pres4);
        root.getChildren().add(pres5);
        root.getChildren().add(pres0);
        
        Scene scene = new Scene(root, 800, 550);
        
        primaryStage.setTitle("Urna Eletrônica");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        launch(args);
    }
}
