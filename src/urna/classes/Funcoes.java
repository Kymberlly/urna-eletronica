package urna.classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Federico
 * @author Kym
 */
public class Funcoes {
    
    private List<Candidato> candidatos = new ArrayList<Candidato> ();
    private List<String>botoes = new ArrayList<String> ();
    

    public void candidatos(){
        
        String[]cv = new String[5];
        int i=0;
        
        try{
            FileReader arq = new FileReader("src\\urna\\dados.txt");
            BufferedReader lerArq = new BufferedReader(arq);
            
            String linha = lerArq.readLine();
            
             while (linha != null) {

                //inserindo todos os candidatos na lista de objetos Candidatos
                cv = linha.split(Pattern.quote("|"));
                candidatos.add(new Candidato(cv[0], cv[1], cv[2], cv[3], cv[4]));
                
                linha = lerArq.readLine();
                i++;
        }
            
            arq.close();
        }
        catch(IOException e){
            System.out.println("Erro na leitura do arquivo. Erro: "+e.getMessage());
        }

        //Visualizacao do primeiro elemento do ArrayList - apenas para visualizacao
        
        System.out.println("--------- Mostrando o array list --------- \n");
        System.out.println("Total de candidatos: "+candidatos.size());
        System.out.println("Nome do candidato: "+candidatos.get(0).getCandidato());
        System.out.println("Cargo: "+candidatos.get(0).getCargo());
        System.out.println("Partido: "+candidatos.get(0).getPartido());
        System.out.println("Numero do candidato: "+candidatos.get(0).getNumCandidato());
        System.out.println("Estado: "+candidatos.get(0).getEstado());
     
    }
    
    
    public void listaPrefeito(){
        System.out.println("\n Lista de Prefeitos: ");
        for(int j=0; j<candidatos.size(); j++){
            
            //Como nao removi o espaco vazio que ficou apos retirar o separador "|"
            //tive que deixar o nome com espaco vazio na frente e atras
            //mesma condicao para os outros dois metodos
            
            if(candidatos.get(j).getCargo().equals(" Prefeito ")){
                System.out.println(candidatos.get(j).getCandidato());
                System.out.println(candidatos.get(j).getCargo());
                System.out.println(candidatos.get(j).getPartido());
                System.out.println(candidatos.get(j).getNumCandidato());
                System.out.println(candidatos.get(j).getEstado());
            }
        }
    }
    
    public void listaPresidente(){
        System.out.println("\n Lista de Presidentes: ");
        for(int j=0; j<candidatos.size(); j++){
            if(candidatos.get(j).getCargo().equals(" Presidente ")){
                System.out.println(candidatos.get(j).getCandidato());
                System.out.println(candidatos.get(j).getCargo());
                System.out.println(candidatos.get(j).getPartido());
                System.out.println(candidatos.get(j).getNumCandidato());
                System.out.println(candidatos.get(j).getEstado());
            }
        }
    }
        
    public void listaDeputado(){
        System.out.println("\n Lista de Prefeito: ");
        for(int j=0; j<candidatos.size(); j++){
            if(candidatos.get(j).getCargo().equals(" Prefeito ")){
                System.out.println(candidatos.get(j).getCandidato());
                System.out.println(candidatos.get(j).getCargo());
                System.out.println(candidatos.get(j).getPartido());
                System.out.println(candidatos.get(j).getNumCandidato());
                System.out.println(candidatos.get(j).getEstado());
            }
        }
    }
    
    
    public void gravar(String botao){
        
    }
    
    
    
    
    public void botoes(String botao, int i, String textFieldText){
        if(botao.equals("confirma")){
            System.out.println(textFieldText);
            System.out.println("confirmado");
        }
    }
    
    
}
