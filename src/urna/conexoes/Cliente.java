package urna.conexoes;

import java.io.*;
import java.util.*;
import java.net.*;

public class Cliente {

    private String host;
    private int porta;
    private String voto;

    public Cliente (String host, int porta, String voto) {
        this.host = host;
        this.porta = porta;
        this.voto = voto;
    }

    public void executa() throws UnknownHostException, IOException {
        
        //criacao do socket
        Socket cliente = new Socket(this.host, this.porta);
        
        if(conectado()){
            System.out.println("O cliente se conectou ao servidor!");
        }
                
        //Send the message to the server
        OutputStream os = cliente.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        String sendMessage = voto + "\n";
        bw.write(sendMessage);
        bw.flush();
        System.out.println("Message sent to the server : "+sendMessage);
        
        os.close();
        osw.close();
        bw.close();
        cliente.close();        
    }
        
    public boolean conectado(){
        return true;
    }
    
}

